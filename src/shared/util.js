import check from 'check-arg-types'

const toType = check.prototype.toType

export function pipe () {
  if (!arguments || !arguments.length) {
    return
  }
  let args = arguments
  let len = args.length
  return function (input) {
    for (let i = 0; i < len; i++) {
      if (!args[i]) continue
      input = args[i](input)
    }
    return input
  }
}

export function caseOn (cases, test) {
  const keys = Object.keys(cases)
  if (test === undefined) {
    return cases.DEFAULT
  }
  for (let x = 0; x < keys.length; x++) {
    let key = keys[x]
    let val = cases[key]
    if (test === key) {
      return toType(val) === 'function'
        ? val()
        : val
    }
  }
  return cases.DEFAULT
}

export function map (fn, ls) {
  if (!fn || !ls) {
    return undefined
  }
  return Array.prototype.map.call(ls, fn)
}

export function filter (fn, ls) {
  if (!fn || !ls) {
    return undefined
  }
  return Array.prototype.filter.call(ls, fn)
}

export function apply () {
  let fn = arguments[0]
  let bound = fn.bind.apply(fn, arguments)
  return bound
}

map.prototype._apply = true
filter.prototype._apply = true
