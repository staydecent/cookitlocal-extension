import check from 'check-arg-types'
import {filter} from 'wasmuth'

import {LMI} from './consts.js'

const INGS = filter((months) => months.length === 24, LMI)

console.log('LMI', INGS)

export function getSeasonalData (recipeIngredients, location = 'washington') {
  if (!recipeIngredients || !recipeIngredients.length) {
    return undefined
  }

  recipeIngredients = recipeIngredients.toLowerCase()

  let {
    mCounts,
    mFoundIngredients
  } = getIngredientCounts(LMI[location], recipeIngredients)
  let mapped = getMappedCounts(mCounts, mFoundIngredients)
  let sorted = sortMappedCounts(mapped)

  if (sorted[0].count === 0) {
    return undefined
  }

  return {
    mapped: mapped,
    sorted: sorted,
    nearestMonth: sorted[0].month,
    isCurrentMonth: sorted[0].index === new Date().getMonth()
  }
}

function getIngredientCounts (months, recipeIngredients) {
  check(arguments, ['array', 'string'])

  let mFoundIngredients = []
  // Each month has an early, and late array
  let mCounts = new Uint8Array(24) // each item is '0'
  let mLen = mCounts.length

  // Iterate each month
  for (let monthIdx = 0; monthIdx < mLen; monthIdx++) {
    let monthsIngredients = months[monthIdx]
    mFoundIngredients[monthIdx] = []

    // Iterate each ingredient for this month
    for (let ingIdx = 0; ingIdx < monthsIngredients.length; ingIdx++) {
      let ing = monthsIngredients[ingIdx]

      if (recipeIngredients.indexOf(ing) !== -1) {
        mFoundIngredients[monthIdx].push(ing)
        mCounts[monthIdx]++
      }
    }
  }

  return {mCounts, mFoundIngredients}
}

function getMappedCounts (mCounts, mFoundIngredients) {
  check(arguments, ['uint8array', 'array'])

  let mLen = mCounts.length

  // Uint8Array.map is not available in Chrome
  let mapped = []
  for (let x = 0; x < mLen; x++) {
    mapped.push({
      index: x,
      month: getMonthStr(x),
      count: mCounts[x],
      ingredients: mFoundIngredients[x]
    })
  }

  return mapped
}

function sortMappedCounts (mapped) {
  check(arguments, ['array'])

  let sorted = []
  for (var x = 0; x < mapped.length; x++) {
    sorted.push(mapped[x])
  }

  let currMonthIdx = getCurrentMonthIdx()
  let relMonthsOrder = startWithIndex(Object.keys(mapped), currMonthIdx)

  relMonthsOrder = relMonthsOrder.map((el) => +el) // Number plz

  sorted.sort((a, b) => {
    if (a.count < b.count) return 1
    if (a.count > b.count) return -1
    let relA = relMonthsOrder.indexOf(a.index)
    let relB = relMonthsOrder.indexOf(b.index)
    if (relA < relB) return -1
    if (relA > relB) return 1
    return 0
  })

  return sorted
}

function getMonthStr (idx) {
  return [
    'Early January',
    'Late January',
    'Early February',
    'Late February',
    'Early March',
    'Late March',
    'Early April',
    'Late April',
    'Early May',
    'Late May',
    'Early June',
    'Late June',
    'Early July',
    'Late July',
    'Early August',
    'Late August',
    'Early September',
    'Late September',
    'Early October',
    'Late October',
    'Early November',
    'Late November',
    'Early December',
    'Late December'
  ][idx]
}

function getCurrentMonthIdx () {
  const currMonthIdx = new Date().getMonth() // getMonth is 0-indexed
  let ret = 0
  for (var x = 0; x < currMonthIdx; x++) {
    ret += 2
  }
  return ret
}

function startWithIndex (arr, idx) {
  let result = []
  let len = arr.length

  for (let x = idx; x < len; x++) {
    result.push(arr[x])
  }

  for (let z = 0; z < idx; z++) {
    result.push(arr[z])
  }

  return result
}
