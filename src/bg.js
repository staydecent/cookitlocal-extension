import {guid} from 'wasmuth'

const db = window.localStorage
const hasGuid = db.hasOwnProperty('guid')
if (!hasGuid) {
  db.setItem('guid', guid())
}
