/* global chrome */
import {init} from 'snabbdom'

import {SCRAPING, ANALYZING} from './shared/consts.js'
import {dispatch, getState, subscribe} from './popover/store.js'
import App from './popover/components/app.js'

const patch = init([
  require('snabbdom/modules/attributes').default,
  require('snabbdom/modules/class').default,
  require('snabbdom/modules/props').default,
  require('snabbdom/modules/style').default,
  require('snabbdom/modules/eventlisteners').default
])

let vnode

// Get the active tab and inject our recipe-scraping code.
chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
  dispatch({ status: SCRAPING })
  chrome.tabs.executeScript(tabs[0].id, {file: 'inject.min.js'})
})

// Wait for the ingredient list message form our injected code.
chrome.runtime.onMessage.addListener((request, sender) => {
  if (!sender.tab) return // Only interested in msg from active tab
  dispatch(request) // pass all properties from tab to our state
  dispatch({ status: ANALYZING })
  console.log('track', {
    guid: window.localStorage.getItem('guid'),
    recipe: request
  })
})

// Mount our App!
window.addEventListener('DOMContentLoaded', () => {
  let node = document.getElementById('app')
  vnode = patch(node, App(getState()))
})

// Diff when state changes
subscribe(() => {
  vnode = patch(vnode, App(getState()))
})
