/* globals chrome HTMLElement */
const idSelector = document.getElementById.bind(document)
const classSelector = document.getElementsByClassName.bind(document)
const allSelector = document.querySelectorAll.bind(document)

const ingredientSelectors = {
  epicurious () {
    return getInnerTexts(classSelector('ingredientsList'))
  },

  food52 () {
    return getInnerTexts(classSelector('recipe-list'))
  },

  marthastewart () {
    return getInnerTexts(classSelector('components-list'))
  },

  thekitchn () {
    return getInnerTexts(idSelector('recipe'))
  },

  generic () {
    const querySelectors = [
      '.ERSIngredients > ul', // Easy Recipe Plugin
      '#zlrecipe-ingredients-list', // ZipList Recipe Plugin
      '.blog-yumprint-ingredients', // yumblog
      '.ingredients-list',
      '.ingredient-list',
      '.ingredient_list',
      '.recipe-list',
      '.ingredients > ul',
      '.entry-content ul',
      '.entry-content'
    ]
    const len = querySelectors.length
    let elms

    for (var x = 0; x < len; x++) {
      let sel = querySelectors[x]
      elms = allSelector(sel)
      if (elms.length >= 1) {
        break
      }
    }

    return getInnerTexts(elms)
  }
}

function getInnerTexts (elms) {
  // Force array
  if (elms instanceof HTMLElement) {
    elms = [elms]
  }

  if (!elms || !elms.length) {
    return undefined
  } else {
    let len = elms.length
    let strs = []

    for (var i = 0; i < len; ++i) {
      let item = elms[i]
      strs.push(item.innerText)
    }

    return strs.join('\n')
  }
}

function getTitle () {
  // adapted from:
  //  https://github.com/ianstormtaylor/metascraper/blob/master/lib/rules/title.js
  let queries = [
    () => document.querySelector('meta[property="og:title"]').content,
    () => document.querySelector('meta[name="twitter:title"]').content,
    () => document.querySelector('meta[name="sailthru.title"]').content,
    () => document.querySelector('.post-title').textContent,
    () => document.querySelector('.entry-title').textContent,
    () => document.querySelector('h1[class*="title"]').textContent,
    () => document.querySelector('title').content
  ]
  let len = queries.length
  let title
  for (var x = 0; x < len; x++) {
    let query = queries[x]
    try {
      title = query()
    } catch (_) {}
    if (title && title.length > 1) {
      break
    }
  }
  return title
}

function getImage () {
  // adapted from:
  //  https://github.com/ianstormtaylor/metascraper/blob/master/lib/rules/image.js
  let queries = [
    () => document.querySelector('meta[property="og:image:secure_url"]').content,
    () => document.querySelector('meta[property="og:image:url"]').content,
    () => document.querySelector('meta[property="og:image"]').content,
    () => document.querySelector('meta[name="twitter:image"]').content,
    () => document.querySelector('meta[property="twitter:image"]').content,
    () => document.querySelector('meta[name="twitter:image:src"]').content,
    () => document.querySelector('meta[name="sailthru.image"]').content,
    () => document.querySelector('meta[name="sailthru.image.full"]').content,
    () => document.querySelector('meta[name="sailthru.image.thumb"]').content,
    () => document.querySelector('article img[src]').getAttribute('src'),
    () => document.querySelector('#content img[src]').getAttribute('src'),
    () => document.querySelector('[class*="article"] img[src]').getAttribute('src'),
    () => document.querySelector('img[src]').getAttribute('src')
  ]
  let len = queries.length
  let imgSrc
  for (var x = 0; x < len; x++) {
    let query = queries[x]
    try {
      imgSrc = query()
    } catch (_) {}
    if (imgSrc && imgSrc.length > 1) {
      break
    }
  }
  return imgSrc
}

// Scrape the page!

let ingredients
let title = getTitle()
let image = getImage()
let siteName = window.location.hostname.replace('www.', '').split('.')[0]

if (siteName in ingredientSelectors) {
  ingredients = ingredientSelectors[siteName]()
} else {
  ingredients = ingredientSelectors.generic()
}

try {
  chrome.runtime.sendMessage({ingredients, title, image, siteName}, () => {})
} catch (_) {
  out = {
    ingredients,
    title,
    image,
    siteName
  }
}
