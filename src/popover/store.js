import atom from 'atom'
import assign from 'object-assign'
import {IDLE} from '../shared/consts.js'

function reducer (action, state) {
  return assign({}, state, action)
}

let initialState = {
  status: IDLE,
  ingredients: []
}

let store = atom(reducer, initialState)

store.subscribe(() => {
  console.log('subscribe', store.getState())
})

export default store
export const subscribe = store.subscribe
export const dispatch = store.dispatch
export const getState = store.getState
