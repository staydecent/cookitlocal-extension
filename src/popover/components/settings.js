import h from 'snabbdom/h'

import {ANALYZING} from '../../shared/consts.js'

export default function (state) {
  let {status, ingredients} = state
  const foundNothing = status === ANALYZING && ingredients.length === 0
  return h('div#settings', {}, [
    h('p.center.intro', {}, [
      (foundNothing) ? 'We didn\'t find any recipes to analyze!' : 'Thanks for using Cook it Local!'
    ]),
    h('ul.months.center', {}, [
      h('li', {}, [
        h('h3.padded', {}, [h('a', {props: {href: '#'}}, 'Your Reminders')])
      ]),
      h('li', {}, [
        h('h3.padded', {}, [h('a', {props: {href: '#'}}, 'Recent Recipes')])
      ]),
      h('li', {}, [
        h('h3.padded', {}, [h('a', {props: {href: '#'}}, 'Feedback')])
      ])
    ])
  ])
}
