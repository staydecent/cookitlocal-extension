import h from 'snabbdom/h'
import SeasonalData from './seasonal.js'
import {dispatch} from '../store.js'
import {ANALYZING, TRANSITIONING, ADD_REMINDER_SCREEN} from '../../shared/consts.js'
import {pipe} from '../../shared/util.js'

const fadeInStyle = {opacity: '0', transition: 'opacity .5s', delayed: {opacity: '1'}}
const onFabClick = (close = false) =>
  dispatch({status: TRANSITIONING, transitionTo: ADD_REMINDER_SCREEN})

export default pipe(
  (state) => ({
    ...state,
    RecipeImage: state.status >= ANALYZING && state.image
      ? h('img', {attrs: {src: state.image}}, [])
      : '',
    Fab: state.status >= ANALYZING
      ? state.transitionTo === 'addReminderScreen'
        ? h('a#fab.close', {on: {click: onFabClick.bind(true)}, tite: 'Set reminder'}, 'x')
        : h('a#fab', {on: {click: onFabClick}, tite: 'Set reminder'}, '+')
      : ''
  }),
  ({RecipeImage, Fab, ...state}) =>
    console.log('app', state) ||
    h('div#app', {style: fadeInStyle}, [
      h('header', {}, [
        h('div.centred', {}, [
          h('h1', {}, ['Cook It Local']),
          h('h2', {}, [state.title || ''])
        ]),
        RecipeImage
      ]),
      Fab,
      SeasonalData(state)
    ])
)
