import h from 'snabbdom/h'

import Settings from './settings.js'
import {getSeasonalData} from '../../shared/seasonalDataService.js'
import {
  SCRAPING,
  ANALYZING,
  TRANSITIONING,
  ADD_REMINDER_SCREEN
} from '../../shared/consts.js'
import {apply, caseOn, pipe} from '../../shared/util.js'

const Status = ({status, ingredients, data, ...state}) =>
  !data
    ? ''
    : caseOn({
      [SCRAPING]: () =>
        h('h3', {}, ['Searching for recipe ingredients...']),
      [ANALYZING]: () =>
        ingredients.length
          ? h('h3', {}, ['Getting seasonal data for the recipe...'])
          : Settings(state),
      DEFAULT: ''
    }, status)

const Reminder = ({data, route}) => apply(caseOn, {
  [ADD_REMINDER_SCREEN]: () =>
    h('div#add-reminder', {
      class: {'move-from-bottom': true}
    }, [
      h('p.center.intro', {}, [
        'This recipe has been saved for later!'
      ]),
      h('ul.months.centred', {}, [
        h('li', {}, [
          h('a.button.button-primary', {href: '#'}, ['View Saved Recipes'])
        ]),
        h('li', {}, [
          h('a.button.button-primary', {href: '#'}, ['Notify Me In ' + data.nearestMonth])
        ]),
        h('li', {}, [
          h('a.button.button-primary', {href: '#'}, ['Email Me In ' + data.nearestMonth])
        ])
      ])
    ]),
  DEFAULT: ''
})(route)

const Ingredients = pipe(
  ({data, status}) => ({
    data,
    status,
    cls: {'scale-down': status === TRANSITIONING}
  }),
  ({data, cls}) => {
    if (!data) return ''
    let listItems = data.sorted.filter((item) => item.count > 0)
    let listElems = listItems.map((item) => {
      return h('li', {}, [
        h('h3', {}, [item.month]),
        h('p', {}, [item.ingredients.join(', ')])
      ])
    })
    return h('div#seasonal-data', {class: cls}, [
      h('p.center.intro', {}, [
        'Most ingredients are available in ',
        h('strong', {}, data.nearestMonth),
        '.'
      ]),
      h('ul.months', {}, listElems)
    ])
  }
)

export default function (state) {
  const data = getSeasonalData(state.ingredients)
  return h('div#main', {}, [
    Status({...state, data}),
    Ingredients({data, status: state.status}),
    Reminder({data, route: state.transitionTo})
  ])
}
