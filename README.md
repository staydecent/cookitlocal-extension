# CookItLocal 

This is a half-working Chrome Extension that, when invoked, extracts any recipe from the current page and analyzes the ingredients for seasonality.

The core scraping logic can be found in [inject.js](https://gitlab.com/staydecent/cookitlocal-extension/-/blob/master/src/inject.js). While the seasonality algorithm can be found in [seasonalDataService.js](https://gitlab.com/staydecent/cookitlocal-extension/-/blob/master/src/shared/seasonalDataService.js).

## Update 2021-03-10

This never saw the light of day, but perhaps the scraping code or seasonalality algorithm could be of to someone. I'd love to hear about any projects that benefitted from this!
