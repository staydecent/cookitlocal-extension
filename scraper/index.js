const fs = require('fs')
const {Chromeless} = require('chromeless')

const file = fs.readFileSync('./src/inject.js', 'utf8')

async function run () {
  const chromeless = new Chromeless()

  const href = 'http://hapaway.com/2017/11/02/grilled-green-bean-salad/'
  const scraped = await chromeless
    .goto(href)
    .evaluate((file) => eval(file), file)

  console.log({...scraped, href})

  await chromeless.end()
}

run().catch(console.error.bind(console))
